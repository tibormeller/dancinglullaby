define(['jquery', 'bootstrap', 'underscore', 'backbone','datepicker'], 
  function (jquery, bootstrap, underscore, backbone, datepicker) {

    "use strict";
    return {

      initApp: function () {

        $(document).ready( function() {
          
          $('.container .input-group.date').datepicker({
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
          });

          $('.container .input-group.date').datepicker('setDate', new Date());    

        });
      
      }
      
    };

})