define(['backbone', 'collections',
  'TopNavigationView', 'MainLayoutView', 'MainEditorView', 'DataTableView'],
function (Backbone, collections,
  TopNavigationView, MainLayoutView, MainEditorView, DataTableView
) {

  'use strict';

  var initMarionetteApp = function () {

    var Router = Backbone.Router.extend({
      routes: {
        '': 'home',
        '/entities': 'entities',
        '/settings': 'settings',
        '/contact': 'contact',
        '/about': 'about'
      },
      initialize: function () {
      }
    });

    var topNavView = new TopNavigationView();

    var router = new Router();
    router.on('route:home', function () {
      var mainLayout = new MainLayoutView();
      var mainEditor = new MainEditorView({
        collection: collections.entities,
        trackedValues: collections.trackedValues
      });
      var dataTable = new DataTableView({ 
        trackedValues: collections.trackedValues
      });
    });

    Backbone.history.start();

  };

  return {
    start: function () {
      initMarionetteApp();
    }

  };

});