define(['jquery', 'underscore', 'backbone', 'datepicker', 'models',
  'text!templates/MainEditorTemplate.html'
  ],
  function($, _, Backbone, datepicker, models, mainEditorTemplate){

    'use strict';
    return Backbone.View.extend({

      el: '#editor',
      events: {
        'click #valueSubmit' : 'valueSubmitted'
      },

      trackedValues: undefined,

      render: function () {
        this.$el.append(this.template);
      },
      
      initialize: function (options) {
        this.trackedValues = options.trackedValues;
        this.template = _.template(mainEditorTemplate, {entitiesList: this.collection.models});
        this.render();
        this.setupDatePicker();
      },

      setupDatePicker: function () {
        $('.container .input-group.date').datepicker({
          todayBtn: 'linked',
          autoclose: true,
          todayHighlight: true
        });

        $('.container .input-group.date').datepicker('setDate', new Date());
      },

      valueSubmitted: function (event) {
        var entity = $('#trackableEntity').val();
        var date = $('#recordDate').val();
        var value = $('#recordValue').val();

        console.log('Adding new item to collection: ' + entity + '; ' + date + '; ' + value);

        this.trackedValues.add(new models.TrackingItem({name: entity, date: date, value: value}));
      }
    });
 
 });
