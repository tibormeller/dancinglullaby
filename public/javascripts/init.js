require.config({
	baseUrl: "javascripts",
	paths: {
		jquery     : "jquery-2.1.0.min",
		bootstrap  : "bootstrap.min",
		underscore : "underscore-min",
		backbone   : "backbone-min",
    marionette : "backbone.marionette.min",
		datepicker : "bootstrap-datepicker",
    dancinglullaby : "dancinglullaby",
    navigation : "navigation/navigation"
	},
	shim: {
		jquery: {
			exports: "$"
		},
		bootstrap : {
      deps: ['jquery']

    },
    backbone : {
      deps : ['jquery', 'underscore'],
      exports : 'Backbone'
    },
    marionette : {
      deps : ['jquery', 'underscore', 'backbone'],
      exports : 'Marionette'
    },
    underscore: {
     exports: "_"
   },
   datepicker: {
     deps: ['jquery'],
     exports: ""
   }
 }
});

require(['jquery', 'bootstrap', 'underscore', 'backbone', 'marionette', 'datepicker', 'dancinglullaby'], 
  function(jquery, bootstrap, underscore, backbone, marionette, datepicker, dancinglullaby){

      dancinglullaby.initApp();

  }
);