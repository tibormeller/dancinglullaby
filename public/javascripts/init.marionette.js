require.config({
	baseUrl: "javascripts",
	paths: {
		jquery     : "libs/jquery-2.1.0.min",
		bootstrap  : "libs/bootstrap.min",
		underscore : "libs/underscore-min",
		backbone   : "libs/backbone-min",
    marionette : "libs/backbone.marionette.min",
		datepicker : "libs/bootstrap-datepicker",
    text       : "libs/text",
    dancinglullaby : "dancinglullaby.marionette",
    TopNavigationView : "navigation/TopNavigationView",
    MainLayoutView : "layouts/MainLayoutView",
    MainEditorView : "editors/MainEditorView",
    DataTableView  : "layouts/DataTableView",
    collections : "models/LullabyModelCollections",
    genData : "models/GeneratedDataCollection",
    models : "models/LullabyModels"
	},
	shim: {
		jquery: {
			exports: "$"
		},
		bootstrap : {
      deps: ['jquery']
    },
    backbone : {
      deps : ['jquery', 'underscore'],
      exports : 'Backbone'
    },
    marionette : {
      deps : ['jquery', 'underscore', 'backbone'],
      exports : 'Marionette'
    },
    underscore: {
     exports: "_"
   },
   datepicker: {
     deps: ['jquery'],
     exports: ""
   }
 }
});

require(['dancinglullaby'], 
  function(dancinglullaby){

      dancinglullaby.start();

  }
);