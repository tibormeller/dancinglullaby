define(['jquery', 'underscore', 'backbone',
  'text!templates/DataTableTemplate.html'
  ],
  function($, _, Backbone, dateTableTemplate){

    'use strict';
    return Backbone.View.extend({

      el: '#monitor',

      trackedValues: undefined,
      
      render: function () {
        this.$el.html(this.template);
      },
      
      initialize: function (options) {
        this.trackedValues = options.trackedValues;
        this.trackedValues.on('add', this.refresh, this);
        this.template = _.template(dateTableTemplate, {trackedValues: this.trackedValues.models});
        this.render();
      },

      refresh : function (event) {
        console.log('paki lulu');
        this.template = _.template(dateTableTemplate, {trackedValues: this.trackedValues.models});
        this.render();
      }
    });

 });
