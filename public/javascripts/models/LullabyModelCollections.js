define(['backbone', 'models'],
  function(Backbone, models){

    'use strict';
    
    var EntitiesCollection = Backbone.Collection.extend({
      model: models.TrackableEntity
    });

    var entities = new EntitiesCollection([
      new models.TrackableEntity({ name: 'Peti' }),
      new models.TrackableEntity({ name: 'Nóri' }),
      new models.TrackableEntity({ name: 'Barni' }),
      new models.TrackableEntity()
      ]);

    var TrackingItemsCollection = Backbone.Collection.extend({
      model: models.TrackingItem
    });

    var trackedValues = new TrackingItemsCollection();

    return {
      
        entities: entities,
        trackedValues: trackedValues

    }

  });
