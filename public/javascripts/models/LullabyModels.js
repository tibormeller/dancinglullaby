define(['backbone'],
	function(Backbone){

		'use strict';
		/* MODELL :) */
return {
    /*
    Backbone's modell definition:

    Models are the heart of any JavaScript application, containing the interactive data as well 
    as a large part of the logic surrounding it: conversions, validations, computed properties, 
    and access control.
    */
    TrackableEntity : Backbone.Model.extend({
      defaults: {
        name: 'Apa'
      }
    }),

    TrackingItem : Backbone.Model.extend({
      defaults: {
        name: 'Apa',
        date: '01/01/1970',
        value: '0'
      }
    })

  };

});