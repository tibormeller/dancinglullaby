define(['jquery', 'underscore', 'backbone',
  'text!templates/TopNavigationTemplate.html'
  ], 
  function($, _, backbone, topNavTemplate){

   return Backbone.View.extend({
    
     el: 'body',
     template: _.template(topNavTemplate, {title: 'Track Everything'}),
     
     render: function () {
       this.$el.append(this.template);
     },

     initialize: function () {
       this.render();
     }
   });

 });
